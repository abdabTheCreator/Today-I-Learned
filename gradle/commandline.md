# Automating gradle commands 

- setup a project: `./gradlew genEclipseRun && ./gradlew build`

- view all tasks available: `./gradlew tasks`

- run a task as an exec: `./gradlew <task-name>`