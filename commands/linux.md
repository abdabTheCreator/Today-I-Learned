# Helpful commands in linux 

knowing how to use the software efficently greatly increases ergonomics of work flow, listed are my most used / most useful 

## Keyboard shortcuts 

`alt + tab` : choose workspace 

## CLI shortcuts
`alt + f`: move one word forward 
`alt + b`: move one word backwards
`ctlr + l`: clear console

## CLI key commands 

`man` : displays the manual for a specified command 
`dpkg`: de-packages a deb file, `-i` to install 
`ctrl + r` : reverse search previous commands 
